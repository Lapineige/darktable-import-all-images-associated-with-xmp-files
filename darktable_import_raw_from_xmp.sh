#!/bin/bash

IFS=$'\n'

echo 'Search for XMP…'

images_files_list=() #array

# By default this script uses 'fd' (https://github.com/sharkdp/fd) instead of 'find'
# To use it with find, you can use 'find . -name .xmp'

for i in $(fd *.xmp)
do
images_files_list+="$(printf %q $(realpath $(printf "${i[@]%.*}")))  "
done

echo 'Import in Darktable…'
echo 'File list: '"${images_files_list[@]}"

$(eval darktable "${images_files_list[@]}") &
