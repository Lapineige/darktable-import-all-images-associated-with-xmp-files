**[Version française]**

# Ce que fais ce script

Ce script importe dans darktable toutes les images (du dossier courant) qui correspondent à un fichier XMP (et non pas la totalité du dossier). Utile si vous voulez importer ces fichiers sans générer de XMP pour les autres (ni les ajouter à la table lumineuse).

# Comment l'installer
- Télécharger le script
- Le rendre exécutable (`chmod +x NomDuFichier`)
- (Optionnel) l'ajouter comme alias pour la ligne de commande dans votre `.bashrc` / `.bash_aliases`: ajouter à la fin du fichier `alias votreAlias="chemin/vers/votre/fichier.sh"`

---
---
---

**[English version]**

# What does this script do ?

This script import in darktable all images (from current directory) which are associated with a XMP file. Usefull if you want to import those files without generating XMP for the other ones (as well as displaying them in the LightTable).

# How to install
- Download the script
- Make it executable (`chmod +x FileName`)
- (Optionnal) add a command line alias in your `.bashrc` / `.bash_aliases`: add `alias yourAlias="chemin/vers/votre/fichier.sh"` at the end of the file
